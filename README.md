## Installation
1. Clone repository
2. Make sure you have [bower](http://bower.io/), [cli](https://www.npmjs.com/package/cli) and  [npm](https://www.npmjs.org/) installed globally
3. On the command prompt go to the project directory and run the following commands:
- `npm install` - bower install is ran from the postinstall
- `gulp` - start local web-server

4.Testing

4.1 Use npm to install Protractor globally (omit the -g if you’d prefer not to install globally):

npm install -g protractor

4.2 To use the Mocha test framework, you'll need to use the BDD interface and Chai assertions with Chai As Promised.
Mocha should be installed in the same place as Protractor - so if protractor was installed globally, install Mocha with -g.

npm install -g mocha
npm install chai
npm install chai-as-promised
You will need to require and set up Chai inside your test files

http://angular.github.io/protractor/#/frameworks

4.3 start selenium in cli webdriver-manager start
4.4 build the preject: gulp
4.5 run tests: gulp test:e2e

known issues:


