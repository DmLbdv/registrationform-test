chai = require 'chai'
chaiAsPromised = require 'chai-as-promised'

chai.use(chaiAsPromised)
expect = chai.expect

describe 'when the user logs in', ->

  # beforeEach ()->
  #   console.log 'spsna bifoich'

  # beforeEach(()->
  #   this.addMatchers({
  #     toHaveClass: (expected)->
  #       @message = ()->
  #         "Expected #{@actual.locator_.value} to have class '#{expected}'"

  #       @actual.getAttribute('class').then((classes)->
  #         classes.split(' ').indexOf(expected) isnt -1
  #       )
  #   })
  # )
  
  hasClass = (element, cls) ->
    element.getAttribute('class').then (classes) ->
      classes.split(' ').indexOf(cls) != -1


  it 'should automatically redirect to "authorization" when location hashfragment is empty', ->
    browser.get('/')
    expect(browser.getTitle()).to.eventually.equal('http://localhost:3001/authorization')
  

  it 'should fill all the inputs', ->
    browser.get('/')

    input = element(`by`.id('login'))
    input.sendKeys 'customer1'
    expect hasClass(input, 'ngValid')


    input = element(`by`.id('email'))
    input.sendKeys 'sth@th.com'
    expect hasClass(input, 'ngValid')


    input = element(`by`.id('password'))
    input.sendKeys 'snththstn'
    expect hasClass(input, 'ngValid')

    input = element(`by`.id('password-conf'))
    input.sendKeys 'snththstn'
    expect hasClass(input, 'ngValid')