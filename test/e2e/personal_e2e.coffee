chai = require 'chai'
chaiAsPromised = require 'chai-as-promised'

chai.use(chaiAsPromised)
expect = chai.expect

describe 'when the user fills out the fields', ->

  tokens = [
    'john',
    'doev',
    '1/2/3456',
    'male',
    'snthtns snth nsh'
  ]
  hasClass = (element, cls) ->
    element.getAttribute('class').then (classes) ->
      classes.split(' ').indexOf(cls) != -1

  it 'should reach "personal" rote and fill all the inputs', ->
    browser.get('/personal')
    expect(browser.getTitle()).to.eventually.equal('http://localhost:3001/personal')

    element.all(`by`.tagName('input')).each (element, index) ->
      element.sendKeys tokens[index]
      expect hasClass(element, 'ngValid')