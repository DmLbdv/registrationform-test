chai = require 'chai'
chaiAsPromised = require 'chai-as-promised'

chai.use(chaiAsPromised)
expect = chai.expect

describe 'when the user fills out the fields', ->

  tokens = [
    '234-2423-24',
    'Mexico',
    'mexico',
    'chkalova. str'
  ]
  hasClass = (element, cls) ->
    element.getAttribute('class').then (classes) ->
      classes.split(' ').indexOf(cls) != -1

  it 'should reach "contacts" rote and fill all the inputs', ->
    browser.get('/contacts')
    expect(browser.getTitle()).to.eventually.equal('http://localhost:3001/contacts')

    element.all(`by`.tagName('input')).each (element, index) ->
      element.sendKeys tokens[index]
      expect hasClass(element, 'ngValid')