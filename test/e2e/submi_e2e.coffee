chai = require 'chai'
chaiAsPromised = require 'chai-as-promised'

chai.use(chaiAsPromised)
expect = chai.expect

describe 'when the user checks and submits', ->

  hasClass = (element, cls) ->
    element.getAttribute('class').then (classes) ->
      classes.split(' ').indexOf(cls) != -1

  it 'should reach "submit" rote and fill all the inputs', ->
    browser.get('/submit')
    expect(browser.getTitle()).to.eventually.equal('http://localhost:3001/submit')

    chkbox = element(`by`.id('sub'))
    if chkbox.isSelected() then null else chkbox.click()
    expect hasClass(chkbox, 'ngValid')